import Navbar from "react-bootstrap/Navbar";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Button from "react-bootstrap/Button"


export default function AppNavbar(){
	return (
		
  <Navbar expand="lg" className="nav navbar navbar-dark mx-auto">
  <Container className="mx-auto">
    <Navbar.Brand href="/" className="text-white">Mathweb</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mx-auto">
        <Nav.Link href="#women" className="text-white mx-5">Women</Nav.Link>
        <Nav.Link href="#men" className="text-white mx-5">Men</Nav.Link>
        <Nav.Link href="#sale" className="text-white mx-5">Sale</Nav.Link>
        <Nav.Link href="/register" className="text-white mx-5">Register</Nav.Link>
        <Nav.Link href="/login" className="text-white mx-5">Login</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>

		)
}
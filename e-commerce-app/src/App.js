import React from "react";

import "./App.css";
import Slogan from "./components/Slogan"
import { Container } from 'react-bootstrap';
import Home from "./pages/Home";
import Register from "./pages/Register";
import { Fragment } from 'react';
import Login from "./pages/Login"
import { Route, Switch } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import AppNavbar from "./AppNavbar"

function App() {
  return (
    
    <Router>
         
         <Container>
           <Switch>
             <Route exact path="/" component={Home} />
             <Route exact path="/login"component={Login} />
             <Route exact path="/register" component={Register} />
           </Switch>
         </Container>
       </Router>
  );
}

export default App;

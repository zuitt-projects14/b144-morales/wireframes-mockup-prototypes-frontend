import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { Row, Col, Card } from "react-bootstrap";

export default function LoginCom(){

const [email, setEmail] = useState('');
const [password, setPassword] = useState('');

	return (
        <Row className = "mt-5 mb-5">
            
            
            <Col xs={12} md={8} className="mx-auto">
        <Card className="cardHighlight p-3">
  <Card.Body>

		<Container>
            <h1>Login</h1>
            <Form >
               <Form.Group className="mb-3" controlId="userEmail">
                   <Form.Label>Email address</Form.Label>
                   <Form.Control 
                       type="email" 
                       placeholder="Enter email" 
                       value={email}
                       onChange={(e) => setEmail(e.target.value)}
                       required
                   />
               </Form.Group>
               <Form.Group className="mb-3" controlId="password">
                   <Form.Label>Password</Form.Label>
                   <Form.Control 
                       type="password" 
                       placeholder="Password"
                       value={password}
                       onChange={(e) => setPassword(e.target.value)}
                       required
                   />
               </Form.Group>
               
                   <Button variant="primary" type="submit" id="submitBtn">
                       Submit
                   </Button>
                   
                           
            </Form> 
        </Container>

  </Card.Body>  
</Card>     
            </Col>
        </Row>

		)
}
import React from 'react';
import { Row, Col, Card } from "react-bootstrap";



export default function Featured() {
  return (
    <Row className="mt-3 mb-3 container-fit justify-content-center">
    <Card.Title>
      <h2 className="text-center my-4 text-white">Featured Products</h2>
    </Card.Title>
      <Col xs={12} md={4}>
          <Card className="cardHighlight p-3">
          <img src={"https://media.istockphoto.com/photos/mens-shortsleeve-tshirt-mockup-in-front-and-back-views-picture-id1328049157?b=1&k=20&m=1328049157&s=170667a&w=0&h=Vr2S5dDVJaUxE8LqQ9q0lc98yNQFfU7rYaUA2rBnphk="} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">Learn from Home</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
</Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
        <img src={"https://mockupsforfree.com/wp-content/uploads/2019/04/02_Realistic_T-Shirt_MockupsForFree.jpg"} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">Study Now, Pay Later</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
  </Card>   


      </Col>


<Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
        <img src={"https://media.istockphoto.com/photos/red-tshirt-clothes-picture-id685779142?k=20&m=685779142&s=612x612&w=0&h=bSAsSmDmvnjz1cBxOfTpVrJ2Nj0c5Yz3A8ovqhjdm_Y="} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">Study Now, Pay Later</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
  </Card>   

  
      </Col>


    </Row>
    )
}



